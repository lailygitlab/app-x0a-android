package nuraini.laily.appx0a

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray


class MainActivity : AppCompatActivity() {
    lateinit var mhsAdapter: AdapterDataMhs
    var daftarMhs = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.159/kampus/show_data.php"
    var url1 = "http://192.168.43.159/Kampus/show_prodi.php"
    var daftarProdi = mutableListOf<String>()
    lateinit var adapProd : ArrayAdapter<String>




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mhsAdapter = AdapterDataMhs(daftarMhs)
        listMhs.layoutManager = LinearLayoutManager(this)
        listMhs.adapter = mhsAdapter
        listMhs.addOnItemTouchListener(itemTouch)
        adapProd = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarProdi)
        spProdi.adapter = adapProd

        showDataMhs()
        getProdi()

    }
    fun getProdi(){
        val request = StringRequest(Request.Method.POST,url1,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarProdi.add(jsonObject.getString("nama_prodi"))
                }
                adapProd.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    val itemTouch = object : RecyclerView.OnItemTouchListener{
        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        }
        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            val view = rv.findChildViewUnder(rv.x,e.y)
            val tag = rv.getChildAdapterPosition(view!!)
            val pos = daftarProdi.indexOf(daftarMhs.get(tag).get("nama_prodi"))

            spProdi.setSelection(pos)
            edNim.setText(daftarMhs.get(tag).get("nim").toString())
            edNama.setText(daftarMhs.get(tag).get("nama").toString())
            Picasso.get().load(daftarMhs.get(tag).get("url")).into(imgUpload)

            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        }

    }


    fun showDataMhs(){
        val request = StringRequest(Request.Method.POST,url,
                Response.Listener { response ->
                    val jsonArray = JSONArray(response)
                    for(x in 0..(jsonArray.length()-1)){
                        val jsonObject = jsonArray.getJSONObject(x)
                        var mhs = HashMap<String,String>()
                        mhs.put("nim",jsonObject.getString("nim"))
                        mhs.put("nama",jsonObject.getString("nama"))
                        mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                        mhs.put("alamat",jsonObject.getString("alamat"))
                        mhs.put("url",jsonObject.getString("url"))
                        daftarMhs.add(mhs)
                    }
                    mhsAdapter.notifyDataSetChanged()
                },
                Response.ErrorListener { error ->
                    Toast.makeText(this, "Terjadi Kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
                })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)

    }
}
